## Образ MySQL
```bash
# Собрать образ
docker build -t isp/percona -f .docker/mysql/Dockerfile .
```

## Образ Redis
```bash
# Собрать образ
docker build -t isp/redis -f .docker/redis/Dockerfile .
```
